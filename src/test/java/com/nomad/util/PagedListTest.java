package com.nomad.util;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class PagedListTest {

	private static final int PAGE_SIZE = 3;
	private PagedList<Integer> p = null;

	@Before
	public void before() {
		List<Integer> l = getArray();
		p = new PagedList<Integer>(PAGE_SIZE, l);
	}

	@Test
	public void testPageSize() {
		assertTrue(p.getPageSize() == PAGE_SIZE);
	}

	@Test
	public void testTotalPages() {
		assertTrue(p.getTotalPages() == 4);
	}

	@Test
	public void testFirst() {
		p.first();
		assertTrue(p.getPageNumber() == 1);
	}

	@Test
	public void testLast() {
		p.last();
		assertTrue(p.getPageNumber() == p.getTotalPages());
	}

	@Test
	public void testHasNext() {
		p.first();
		assertTrue(p.hasNext());
		p.last();
		assertFalse(p.hasNext());
	}

	@Test
	public void testNext() {
		p.first();
		assertTrue(p.getPageNumber() == 1);
		List<Integer> l = p.next();
		System.out.println(l);
		assertTrue(l.size() == PAGE_SIZE);
		assertTrue(p.getPageNumber() == 2);
		l = p.next();
		System.out.println(l);
		l = p.next();
		System.out.println(l);
		assertTrue(p.getPageNumber() == 4);
		l = p.next();
		System.out.println(l);
		assertTrue(p.getPageNumber() == 4);
	}

	private static List<Integer> getArray() {
		Random r = new Random();
		return r.ints(10).map(i -> i).collect(ArrayList::new, ArrayList::add,
				ArrayList::addAll);
	}

}
