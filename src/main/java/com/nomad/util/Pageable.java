package com.nomad.util;

import java.util.List;

public interface Pageable<E> {
	
	int DEFAULT_PAGE_SIZE=10;
	
	int getPageSize();
	
	void setPageSize(int size);
	
	int getPageNumber();
	
	void first();
	
	void last();
	
	boolean hasNext();
	
	List<E> next();
	
	int getTotalPages();
	
}
