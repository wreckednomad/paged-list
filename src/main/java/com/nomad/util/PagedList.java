package com.nomad.util;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class PagedList<E> extends ArrayList<E> implements Pageable<E> {

	private int currentPageNumber = -1;

	private int totalPages = 0;

	private int cursor = -1;

	private int pageSize = 0;

	private void init() {
		this.totalPages = calculateTotalPages();
		this.currentPageNumber = 1;
		this.cursor = 1;
	}

	private int calculateTotalPages() {
		int size = this.size();
		if (size % this.pageSize == 0)
			return size / this.pageSize;
		return (size / this.pageSize) + 1;
	}

	public PagedList(List<E> content) {
		super(content);
		this.pageSize = DEFAULT_PAGE_SIZE;
		init();
	}

	public PagedList(int pageSize, List<E> content) {
		super(content);
		this.pageSize = pageSize;
		init();
	}

	@Override
	public int getPageSize() {
		return this.pageSize;
	}

	@Override
	public void setPageSize(int size) {
		this.pageSize = size;
	}

	@Override
	public int getPageNumber() {
		return this.currentPageNumber;
	}

	@Override
	public void first() {
		this.cursor = 1;
		this.currentPageNumber = 1;
	}

	@Override
	public void last() {
		int size = this.size();
		this.cursor = ((size / pageSize) * pageSize) + 1;
		this.currentPageNumber = calcualatePageNumber();
	}

	@Override
	public boolean hasNext() {
		if (this.currentPageNumber + 1 > totalPages) {
			return false;
		}
		return true;
	}

	@Override
	public List<E> next() {
		if (isLastPage()) {
			return this.subList(cursor - 1, this.size());
		}
		int index = cursor - 1;
		List<E> list = this.subList(index, index + pageSize);
		cursor += pageSize;
		this.currentPageNumber = calcualatePageNumber();
		return list;
	}

	private boolean isLastPage() {
		int currentPage = calcualatePageNumber();
		return currentPage == totalPages;
	}

	@Override
	public int getTotalPages() {
		return this.totalPages;
	}

	private int calcualatePageNumber() {
		if (cursor % pageSize == 0)
			return cursor / pageSize;
		return (cursor / pageSize) + 1;
	}
}
